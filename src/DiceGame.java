public class DiceGame {
      int diceFace;


    public void showDice () {

        switch (diceFace) {
            case 1:

                System.out.println(" " + "   " + " ");
                System.out.println(" " + " * " + " ");
                System.out.println(" " + "   " + " ");
                break;
            case 2:

                System.out.println(" " + " * " + " ");
                System.out.println(" " + "   " + " ");
                System.out.println(" " + " * " + " ");
                break;
            case 3:

                System.out.println("*" + "   " + " ");
                System.out.println(" " + " * " + " ");
                System.out.println(" " + "   " + "*");
                break;
            case 4:

                System.out.println("*" + "   " + "*");
                System.out.println(" " + "   " + " ");
                System.out.println("*" + "   " + "*");
                break;
            case 5:

                System.out.println("*" + "   " + "*");
                System.out.println(" " + " * " + " ");
                System.out.println("*" + "   " + "*");
                break;
            case 6:

                System.out.println("*" + " * " + "*");
                System.out.println(" " + "   " + " ");
                System.out.println("*" + " * " + "*");
                break;
            default:

                System.out.println("Invalid throw. The dice is not on the table!");
        }


    }


}
