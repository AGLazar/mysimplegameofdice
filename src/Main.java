
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Players player1 = new Players();
        Players player2 = new Players();

        player1.logIn();
        player2.logIn();

        //Tis is the age test//
        if( player1.age >= 18 && player2.age >= 18) {

            System.out.println("The dice has been cast. "+ player1.name +" : ");
            Random numberFrom1To6 = new Random();
            DiceGame dice1 = new DiceGame();
            dice1.diceFace = numberFrom1To6.nextInt(6);
            dice1.showDice();

            System.out.println("The dice has been cast. "+ player2.name +" : ");
            DiceGame dice2 = new DiceGame();
            dice2.diceFace = numberFrom1To6.nextInt(6);
            dice2.showDice();

            if (dice1.diceFace > dice2.diceFace && dice1.diceFace!=0 && dice2.diceFace!=0){
                System.out.println(player1.name + " is the winner!");
            }

            if( dice2.diceFace > dice1.diceFace && dice1.diceFace!=0 && dice2.diceFace!=0){
                System.out.println(player2.name+ " is the winner!");
            } else {
                System.out.println("Nowbody wins. This is a tie.");
            }



        } else {
            System.out.println("Person under 18 years old are not allowed to play dice. It's bad for you.");
        }


    }
}
